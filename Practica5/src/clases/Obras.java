package clases;

public class Obras {


	private String autor;
	private String titulo;
	private double precio;
	private double altura;
	private	double ancho;
	
	public Obras(String autor, String titulo, double altura, double ancho, double precio) {
		this.autor = autor;
		this.titulo = titulo;
		this.altura = altura;
		this.ancho  = ancho;
		this.precio = precio;
	}
	@Override
	public String toString() {
		return "Obras [autor=" + autor + ", titulo=" + titulo + ", precio=" + precio + ", altura=" + altura + ", ancho="
				+ ancho + "]";
	}
	public String getAutor() {
		return autor;
	}
	public void setAutor(String autor) {
		this.autor = autor;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public double getPrecio() {
		return precio;
	}
	public void setPrecio(double precio) {
		this.precio = precio;
	}
	public double getAltura() {
		return altura;
	}
	public void setAltura(double altura) {
		this.altura = altura;
	}
	public double getAncho() {
		return ancho;
	}
	public void setAncho(double ancho) {
		this.ancho = ancho;
	}
}
