package clases;

public class Cuadro extends Obras{

	
	private String color;
	private String epoca;


	public Cuadro(String autor, String titulo, String color, double altura, double ancho, double precio,String epoca) {
		super(autor,titulo,altura,ancho,precio);
		this.color = color;
		this.epoca = epoca;
	}

	public String getColor() {
		return color;
	}


	public void setColor(String color) {
		this.color = color;
	}
	
	public String getEpoca() {
		return epoca;
	}

	public void setEpoca(String epoca) {
		this.epoca = epoca;
	}

	@Override
	public String toString() {
		return   super.toString()+"Cuadro [color=" + color + ", epoca=" + epoca + "]";
	}
		
}

