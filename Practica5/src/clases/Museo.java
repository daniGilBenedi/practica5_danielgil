package clases;

import java.util.Scanner;

public class Museo {
	
	Cuadro[] cuadro;
	Escultura[] escultura;
	Comprador[] comprador;
	

	public Museo(int cantidadEsculturas, int cantidadCompradores, int cantidadCuadros) {
		
		this.escultura = new Escultura[cantidadEsculturas];
		this.comprador = new Comprador[cantidadCompradores];
		this.cuadro = new Cuadro[cantidadCuadros];
	}
	/*Comprador*/
	public Comprador[] altaComprador(String nombre, int edad, String DNI) {
		
		for (int i = 0; i < comprador.length; i++) {
			
			if(comprador[i] == null) {
				comprador[i] = new Comprador(nombre,edad,DNI);
				
				return comprador;
			}
		}
		return null;
	}
	public void listarGeneralComprador() {
		
		for (int i = 0; i < comprador.length; i++) {
			
			if(comprador[i] != null) {
				
				System.out.println(comprador[i].toString());
			}
		}
	}
	/*Escultura*/
	public Escultura[] altaEscultura(String autor, String titulo, double altura, double ancho, double precio, double peso, String material) {
		
		for (int i = 0; i < escultura.length; i++) {
			
			if(escultura[i] == null) {
				escultura[i] = new Escultura(autor,titulo,altura, ancho, precio,peso,material);
				
				return escultura;
			}
		}
		return null;
	}
	
	public Escultura buscarEscultura(String titulo) {
		
		for (int i = 0; i < escultura.length; i++) {
					
					if(escultura[i] != null) {
						
						if(titulo.equals(escultura[i].getTitulo())) {
							
							return escultura[i];
						}	
					}
				}
				return null;
			}
	
	public void eliminarEscultura(String titulo) {
		
		for (int i = 0; i < escultura.length; i++) {
			
			if(escultura[i] != null) {
				
				if(titulo.equals(escultura[i].getTitulo())) {
					
					escultura[i] = null;
				}
			}
		}
		
	}
	
	public void listarGeneralEscultura() {
		
		for (int i = 0; i < escultura.length; i++) {
			
			if(escultura[i] != null) {
				
				System.out.println(escultura[i].toString());
			}
		}
	}
	
	
	public void cambiarEscultura(String autor, String tituloCambiar, String titulo, double altura, double ancho, double precio, double peso, String material) {
		
		for (int i = 0; i < escultura.length; i++) {
			
			if(escultura[i] != null) {
				
				if(titulo.equals(escultura[i].getTitulo())) {
					escultura[i].setAutor(autor);
					escultura[i].setTitulo(tituloCambiar);
					escultura[i].setPeso(peso);
					escultura[i].setAltura(altura);
					escultura[i].setAncho(ancho);
					escultura[i].setPrecio(precio);
					escultura[i].setMaterial(material);
					System.out.println("Cambios realizados con exito!!");
					System.out.println(cuadro[i].toString());
				}

			}
		}
	}
	
	
	public void listarAtributoEscultura(String titulo) {
		
		for (int i = 0; i < escultura.length; i++) {
				
			if(escultura[i] != null) {
					
				if(titulo.equals(escultura[i].getTitulo())) {
					
					System.out.println(escultura[i]);
				}
				
			}
		}
	}
	
	public void dimensionEscultura(String titulo) {
		
		double dimension = 0;
	
		for (int i = 0; i < escultura.length; i++) {
			
			if(escultura[i] != null) {
				
				if(titulo.equals(escultura[i].getTitulo())) {
					
					dimension = escultura[i].getAltura()*escultura[i].getAncho();
					System.out.println("La dimensión del cuadro " + escultura[i].getTitulo()+ " es " + dimension + " cm2");
				}
			}
		}
	}
	
	
	public void totalPrecioObrasMuseoEscultura() {
		double precioTotalObras = 0;
		for (int i = 0; i < escultura.length; i++) {
			
			if(escultura[i] != null) {
				
				precioTotalObras += escultura[i].getPrecio();
				
			}
		}
		System.out.println("El precio total de las obras del museo es " + precioTotalObras);
	}
	public void comprarEscultura() {
		int poscionComprador = 0;
		
		@SuppressWarnings("resource")
		Scanner in = new Scanner(System.in);
		System.out.println("Elige el nombre del comprador que va a realizar la compra");
		listarGeneralComprador();
		String nombreComprador = in.nextLine(); 
		System.out.println("Escribe uno de los siguientes nombres de esculturas para comprarlo");
		listarGeneralEscultura();
		
	
		String esculturasParaComprar = in.nextLine();
		
		
		for (int i = 0; i < comprador.length; i++) {
			
			if(comprador[i] != null) {
				
				if(nombreComprador.equalsIgnoreCase(comprador[i].getNombre())) {
					poscionComprador = i;
				}
			}
			
		}
		
		for (int i = 0; i < escultura.length; i++) {
			
			if(escultura[i] != null) {
				
				if(esculturasParaComprar.equalsIgnoreCase(escultura[i].getTitulo())) {
					
					comprador[poscionComprador].setObrasCompradas(esculturasParaComprar);
				}
				
			}
		}
	}
	
	/*Cuadro*/
	public Cuadro[] altaCuadro(String autor, String titulo, String color, double altura, double ancho, double precio, String epoca) {
		
		for (int i = 0; i < cuadro.length; i++) {
			
			if(cuadro[i] == null) {
				cuadro[i] = new Cuadro(autor,titulo,color,altura, ancho, precio,epoca);
				
				return cuadro;
			}
		}
		return null;
	}
	
	public Cuadro buscar(String titulo) {
		
		for (int i = 0; i < cuadro.length; i++) {
					
					if(cuadro[i] != null) {
						
						if(titulo.equals(cuadro[i].getTitulo())) {
							
							return cuadro[i];
						}	
					}
				}
				return null;
			}
	
	public void eliminar(String titulo) {
		
		for (int i = 0; i < cuadro.length; i++) {
			
			if(cuadro[i] != null) {
				
				if(titulo.equals(cuadro[i].getTitulo())) {
					
					cuadro[i] = null;
				}
			}
		}
		
	}
	
	public void listarGeneralCuadro() {
		
		for (int i = 0; i < cuadro.length; i++) {
			
			if(cuadro[i] != null) {
				
				System.out.println(cuadro[i].toString());
			}
		}
	}
	
	
	public void cambiar(String tituloCambiar, String autor, String titulo, String color, double altura, double ancho, double precio,String epoca) {
		
		for (int i = 0; i < cuadro.length; i++) {
			
			if(cuadro[i] != null) {
				
				if(titulo.equals(cuadro[i].getTitulo())) {
					cuadro[i].setAutor(autor);
					cuadro[i].setTitulo(tituloCambiar);
					cuadro[i].setColor(color);
					cuadro[i].setAltura(altura);
					cuadro[i].setAncho(ancho);
					cuadro[i].setPrecio(precio);
					cuadro[i].setEpoca(epoca);
					System.out.println("Cambios realizados con exito!!");
					System.out.println(cuadro[i].toString());
				}

			}
		}
	}
	
	
	public void listarAtributo(String titulo) {
		
		for (int i = 0; i < cuadro.length; i++) {
				
			if(cuadro[i] != null) {
					
				if(titulo.equals(cuadro[i].getTitulo())) {
					
					System.out.println(cuadro[i]);
				}
				
			}
		}
	}
	
	public void dimension(String titulo) {
		
		double dimension = 0;
	
		for (int i = 0; i < cuadro.length; i++) {
			
			if(cuadro[i] != null) {
				
				if(titulo.equals(cuadro[i].getTitulo())) {
					
					dimension = cuadro[i].getAltura()*cuadro[i].getAncho();
					System.out.println("La dimensión del cuadro " + cuadro[i].getTitulo()+ " es " + dimension + " cm2");
				}
			}
		}
	}
	
	public void comprarCuadro() {
		int poscionComprador = 0;
		
		@SuppressWarnings("resource")
		Scanner in = new Scanner(System.in);
		System.out.println("Elige el nombre del comprador que va a realizar la compra");
		listarGeneralComprador();
		String nombreComprador = in.nextLine(); 
		System.out.println("Escribe uno de los siguientes nombres de cuadros para comprarlo");
		listarGeneralCuadro();
		
	
		String cuadrosParaComprar = in.nextLine();
		
		
		for (int i = 0; i < comprador.length; i++) {
			
			if(comprador[i] != null) {
				
				if(nombreComprador.equalsIgnoreCase(comprador[i].getNombre())) {
					poscionComprador = i;
				}
			}
			
		}
		
		for (int i = 0; i < cuadro.length; i++) {
			
			if(cuadro[i] != null) {
				
				if(cuadrosParaComprar.equalsIgnoreCase(cuadro[i].getTitulo())) {
					
					comprador[poscionComprador].setObrasCompradas(cuadrosParaComprar);
				}
				
			}
		}
	}
	
	
	
	public void totalPrecioObrasMuseo() {
		double precioTotalObras = 0;
		for (int i = 0; i < cuadro.length; i++) {
			
			if(cuadro[i] != null) {
				
				precioTotalObras += cuadro[i].getPrecio();
				
			}
		}
		System.out.println("El precio total de las obras del museo es " + precioTotalObras);
	}
}
