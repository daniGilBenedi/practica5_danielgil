package clases;

public class Escultura extends Obras {

	private double peso;
	private String material;
	
	
	public Escultura(String autor, String titulo, double altura, double ancho, double precio, double peso, String material) {
		super(autor, titulo, altura, ancho, precio);
		this.peso = peso;
		this.material = material;
	}
	

	public double getPeso() {
		return peso;
	}
	public void setPeso(double peso) {
		this.peso = peso;
	}
	public String getMaterial() {
		return material;
	}
	public void setMaterial(String material) {
		this.material = material;
	}

	
}
